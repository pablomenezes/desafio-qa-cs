class Price

  def initialize(price, special_price = nil, special_qty = nil)
    @price = price
    @special_price = special_price
    @special_qty = special_qty
  end

  def total_price_item(quantity)
    value = 0

    if @special_price != nil
      value = value + calculate_discounts(quantity)
    else
      value = value + quantity * @price
    end
    value
  end

  def calculate_discounts(quantity)
    @special_price * (quantity / @special_qty) + @price * (quantity % @special_qty)
  end

end

# ITEM => (Price, Special Prize, Qty for Special Prize)
RULES = {
  'A' => Price.new(50, 130, 3),
  'B' => Price.new(30, 45, 2),
  'C' => Price.new(20),
  'D' => Price.new(15),
}

class CheckOut

  def initialize(rules)
    @rules = RULES
    @cart = Hash.new {0}
  end

  def total
    result = 0
    result = @cart.inject(0) { |sum, (item,qtd) | sum + total_price_item(item, qtd) }

    puts "The total is #{result}"

    result
  end

  def scan(item)
    puts "adding item #{item} to the cart"
    @cart[item] += 1
  end

  private

  def total_price_item(item,quantity)
    @rules[item].total_price_item(quantity)
  end
end
