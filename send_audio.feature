Funcionalidade: Enviar áudio

  Como usuário do Whatsapp
  Eu quero poder enviar áudios em uma conversa
  Para não necessitar digitar textos muito grandes em uma conversa

  Cenário: Enviando audio
    Dado que eu esteja em uma conversa com um usuário
    E eu pressione e segure o ícone de áudio por 1 segundo ou mais
    Quando eu soltar o ícone de áudio
    Então o meu áudio deve ser enviado para o usuário

  Cenário: Cancelar a gravação do audio
    Dado que eu esteja em uma conversa com um usuário
    E eu pressione e segure o ícone de áudio por 1 segundo ou mais
    Quando eu quiser cancelar a gravação do áudio
    Então eu deslizo o ícone de áudio para a esquerda
    E o áudio não deve aparecer na conversa

  Cenário: Cancelar o envio do áudio
    Dado que eu esteja em uma conversa com um usuário
    E eu pressione e segure o ícone de áudio por 1 segundo ou mais
    Quando eu soltar o ícone de áudio
    Então eu devo clicar no ícone de cancelar upload do áudio
    E o outro usuário não deve receber o áudio
