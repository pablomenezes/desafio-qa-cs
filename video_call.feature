Funcionalidade: Realizar uma chamada de vídeo

  Como usuário do Whatsapp
  Eu quero realizar uma chamada de vídeo com outro usuário
  Para conversar com outros usuários de forma mais aproximada

  Cenário: Realizar chamada
    Dado que eu esteja na lista de conversas do Whatsapp
    E eu clique em Chamadas
    E clique no ícone de realizar nova chamada
    E selecione um usuário
    Quando eu clicar no ícone de chamada em vídeo
    Então a chamada deve ser realizada

  Cenário: Encerrar chamada
    Dado que eu esteja em uma chamada de vídeo com outro usuário
    Quando eu clicar no ícone de Encerrar Chamada
    Então eu devo voltar para a lista de chamadas
    E a chamada deve estar realizada na lista de chamadas

  Cenário: Alterar câmera
    Dado que eu esteja em uma chamada de vídeo com outro usuário
    Quando eu clicar no ícone de Alterar Camêra
    Então eu devo ver a imagem da outra Camêra

  Cenário: Abrir conversa
    Dado que eu esteja em uma chamada de vídeo com outro usuário
    Quando eu clicar no ícone de Abrir Conversa
    Então eu devo ver a conversa
    E uma faixa clicável no topo da tela para retornar a chamada de vídeo

  Cenário: Silenciar a chamada de vídeo
    Dado que eu esteja em uma chamada de vídeo com outro usuário
    Quando eu clicar no ícone de Silenciar Chamada
    Então o outro usuário não deve ouvir a minha voz
